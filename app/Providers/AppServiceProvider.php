<?php

namespace Koala\Providers;

use View;
use Carbon\Carbon;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        cache()->rememberForever('leaveTypes', function() {
            return LeaveType::orderBy('id')->get();
        });

        View::composer('layouts.navbar', function($view) {
            $view->with('leaveRequestsCount', auth()->user() && auth()->user()->type === 'admin' ? LeaveRequest::requests()->count() : 0);
        });

        View::composer('leaveRequests._form', function($view) {
            $leaveTypes = cache('leaveTypes');
            $view->with('leaveTypes', (clone $leaveTypes)->pluck('title', 'id'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('development', 'local', 'testing')) {
            $this->app->register(\PrettyRoutes\ServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            
            $this->app->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        }

        Carbon::setWeekendDays([
            Carbon::SATURDAY,
            Carbon::FRIDAY
        ]);
    }
}
