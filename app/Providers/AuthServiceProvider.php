<?php

namespace Koala\Providers;

use Koala\Message;
use Koala\LeaveRequest;
use Koala\Policies\MessagePolicy;
use Illuminate\Support\Facades\Gate;
use Koala\Policies\LeaveRequestPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        LeaveRequest::class => LeaveRequestPolicy::class,
        Message::class => MessagePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-profile', function($user, $profile) {
            return $user->type === 'admin' || $user->id === $profile->id;
        });
    }
}
