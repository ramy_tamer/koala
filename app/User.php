<?php

namespace Koala;

use Koala\Message;
use Koala\LeaveRequest;
use Koala\WeekDaysDuration;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'name', 'avatar', 'google_id', 'google_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function leaveRequests()
    {
        return $this->hasMany(LeaveRequest::class, 'employee_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function getCalculatedUnpaidLeavesAttribute()
    {
        return $this->leavesCount(LeaveType::unpaid());
    }

    public function getCalculatedNormalLeavesAttribute()
    {
        return 14 - $this->leaveRequests()
                             ->where('leave_requests.status', 'accepted')
                             ->join('leave_types', 'leave_types.id', '=', 'leave_requests.leave_type_id')
                             ->where('leave_types.title', 'Normal')
                             ->select('leave_requests.leave_start', 'leave_requests.leave_end')
                             ->get()
                             ->map(function($item) {
                                return WeekDaysDuration::get($item);
                             })
                             ->sum();
    }

    public function getCalculatedCasualLeavesAttribute()
    {
        return 7 - $this->leaveRequests()
                        ->where('leave_requests.status', 'accepted')
                        ->join('leave_types', 'leave_types.id', '=', 'leave_requests.leave_type_id')
                        ->where('leave_types.title', 'Casual')
                        ->select('leave_requests.leave_start', 'leave_requests.leave_end')
                        ->get()
                        ->map(function($item) {
                            return WeekDaysDuration::get($item);
                        })
                        ->sum();
    }

    public function leavesCount(LeaveType $leaveType)
    {
        $sum = $leaveType->max_number - $this->leaveRequests()
                                             ->where('leave_requests.status', 'accepted')
                                             ->join('leave_types', 'leave_types.id', '=', 'leave_requests.leave_type_id')
                                             ->where('leave_types.id', $leaveType->id)
                                             ->select('leave_requests.leave_start', 'leave_requests.leave_end')
                                             ->get()
                                             ->map(function($item) {
                                                 return WeekDaysDuration::get($item);
                                             })
                                             ->sum();
        
        return abs($sum);
    }

}
