<?php

namespace Koala;

use Koala\LeaveRequest;
use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $fillable = ['title', 'is_paid', 'auto_accept', 'max_number'];

    public function leaveRequests()
    {
    	return $this->hasMany(LeaveRequest::class);
    }

    public static function normal()
    {
    	return cache()->rememberForever('normalType', function() { return LeaveType::whereTitle('normal')->first(); });;
    }

    public static function casual()
    {
    	return cache()->rememberForever('casualType', function() { return LeaveType::whereTitle('casual')->first(); });;
    }

    public static function unpaid()
    {
    	return cache()->rememberForever('unpaidType', function() { return LeaveType::whereTitle('unpaid')->first(); });
    }

    public function __toString()
    {	
    	return sprintf("%s Leave [%s]", $this->title, $this->is_paid ? 'Paid': 'Unpaid');
    }
}
