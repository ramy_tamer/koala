<?php

namespace Koala\Listeners;

use Koala\User;
use Koala\LeaveType;
use Koala\WeekDaysDuration;
use Koala\Events\LeaveRequestCreated;
use Koala\Events\LeaveRequestAccepted;
use Koala\Events\LeaveRequestRejected;
use Illuminate\Support\Facades\Notification;
use Koala\Notifications\EmployeeRequestedLeave;
use Koala\Notifications\AdminAcceptedLeaveRequest;
use Koala\Notifications\AdminRejectedLeaveRequest;

class LeaveRequestEventSubscriber
{
    public function updateEmployeeLeaves($event)
    {
        if($event->leaveRequest->status === 'accepted') {
            $employee = $event->leaveRequest->employee;
            $days     = WeekDaysDuration::get($event->leaveRequest);

            if(! $event->leaveRequest->type->is_paid) {
                $employee->unpaid_leaves += $days;
            }

            switch($event->leaveRequest->type->id) {
                case LeaveType::normal()->id:
                    $employee->normal_leaves -= $days;
                    break;
                case LeaveType::casual()->id:
                    $employee->casual_leaves -= $days;
                    break;
            }
            
            $employee->save();
        }
    }

    public function notifyAdmins($event)
    {
        $admins = User::where('type', 'admin')->get();
        Notification::send($admins, new EmployeeRequestedLeave($event->leaveRequest));
    }

    public function notifyEmployeeForAcceptance($event)
    {
        $event->leaveRequest->employee->notify(new AdminAcceptedLeaveRequest($event->leaveRequest));
    }

    public function notifyEmployeeForRejection($event)
    {
        $event->leaveRequest->employee->notify(new AdminRejectedLeaveRequest($event->leaveRequest));   
    }
    
    /**
     * Register the listerers for the subscriber.
     * 
     * @param  Illuminate\Events\Dispatcher $events
     * @return void       
     */
    public function subscribe($events)
    {
        $events->listen([
            LeaveRequestCreated::class,
            LeaveRequestAccepted::class
        ], 'Koala\Listeners\LeaveRequestEventSubscriber@updateEmployeeLeaves');

        $events->listen([
            LeaveRequestCreated::class,
        ], 'Koala\Listeners\LeaveRequestEventSubscriber@notifyAdmins');

        $events->listen([
            LeaveRequestAccepted::class,
        ], 'Koala\Listeners\LeaveRequestEventSubscriber@notifyEmployeeForAcceptance');

        $events->listen([
            LeaveRequestRejected::class,
        ], 'Koala\Listeners\LeaveRequestEventSubscriber@notifyEmployeeForRejection');
       
    }
}
