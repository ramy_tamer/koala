<?php

namespace Koala\Notifications;

use Koala\LeaveRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EmployeeRequestedLeave extends Notification implements ShouldQueue
{
    use Queueable;

    public $leaveRequest;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(LeaveRequest $leaveRequest)
    {
        $this->leaveRequest = $leaveRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $type     = $this->leaveRequest->type;
        $employee = $this->leaveRequest->employee;
        $duration = sprintf("%d %s", $this->leaveRequest->duration, str_plural('day', $this->leaveRequest->duration));
        $start = sprintf("%s [%s]", $this->leaveRequest->leave_start->diffForHumans(), $this->leaveRequest->leave_start->toDateString());

        return (new MailMessage)
                    ->greeting("Hello, {$notifiable->name} !")
                    ->subject('New Leave Request')
                    ->line("Employee '{$employee->name}' requested a {$type} leave for {$duration}, starting {$start}.")
                    ->action('Check the request from here', route('leave-request.show', $this->leaveRequest->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @codeCoverageIgnore
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
