<?php

namespace Koala\Notifications;

use Koala\LeaveRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminRejectedLeaveRequest extends Notification implements ShouldQueue
{
    use Queueable;

    public $leaveRequest;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(LeaveRequest $leaveRequest)
    {
        $this->leaveRequest = $leaveRequest;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $start = $this->leaveRequest->leave_start->toDateString();
        $end   = $this->leaveRequest->leave_end->toDateString();
        
        return (new MailMessage)
                    ->error()
                    ->greeting("Hello, {$notifiable->name} !")
                    ->subject('Rejected Leave Request')
                    ->line("Your {$this->leaveRequest->type} leave request from {$start} to {$end} has been Rejected by admin '{$this->leaveRequest->admin->name}'")
                    ->line('Thank you for using Koala!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @codeCoverageIgnore
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
