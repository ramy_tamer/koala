<?php

namespace Koala\Policies;

use Koala\User;
use Carbon\Carbon;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Illuminate\Http\Request;
use Koala\Policies\LeaveRequestChecker;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeaveRequestPolicy
{
    use HandlesAuthorization;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function before($user, $ability)
    {
        if ($user->type === 'admin' && ! in_array($ability, ['respond', 'delete'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the leaveRequest.
     *
     * @param  \Koala\User  $user
     * @param  \Koala\LeaveRequest  $leaveRequest
     * @return mixed
     */
    public function list(User $user)
    {
        return $user->type === 'admin';
    }

    /**
     * Determine whether the user can view the leaveRequest.
     *
     * @param  \Koala\User  $user
     * @param  \Koala\LeaveRequest  $leaveRequest
     * @return mixed
     */
    public function view(User $user, LeaveRequest $leaveRequest)
    {
        return $user->id === $leaveRequest->employee_id;
    }

    /**
     * Determine whether the user can create leaveRequests.
     *
     * @param  \Koala\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $leaveType = LeaveType::find($this->request->leave_type_id);

        if(! $leaveType) {
            return false;
        }

        return (new LeaveRequestChecker($leaveType))->check($user);

    }

    /**
     * Determine whether the user can respond the leaveRequest.
     *
     * @param  \Koala\User  $user
     * @param  \Koala\LeaveRequest  $leaveRequest
     * @return mixed
     */
    public function respond(User $user, LeaveRequest $leaveRequest)
    {
        return $user->type === 'admin' && $leaveRequest->status === 'reviewing';
    }

    /**
     * Determine whether the user can delete the leaveRequest.
     *
     * @param  \Koala\User  $user
     * @param  \Koala\LeaveRequest  $leaveRequest
     * @return mixed
     */
    public function delete(User $user, LeaveRequest $leaveRequest)
    {
        return 
            ($leaveRequest->employee_id === $user->id) &&
            $leaveRequest->status === 'reviewing';
    }
}
