<?php

namespace Koala\Policies;

use Koala\User;
use Koala\LeaveRequest;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    protected $request;

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function before($user, $ability)
    {
        if ($user->type === 'admin') {
            return true;
        }
    }

    public function create(User $user)
    {
        $leaveRequest = LeaveRequest::findOrFail($this->request->leave_request_id);

        return $leaveRequest->employee_id === $user->id;
    }
}
