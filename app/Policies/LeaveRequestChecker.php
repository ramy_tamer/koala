<?php

namespace Koala\Policies;

use Koala\User;
use Koala\LeaveType;
use Koala\WeekDaysDuration;
use Illuminate\Http\Request;

class LeaveRequestChecker
{
	protected $request;

	protected $weekDaysDuration;

	protected $leaveType;

	function __construct(LeaveType $leaveType)
	{
		$this->request = app()->make(Request::class);

		$this->weekDaysDuration = WeekDaysDuration::get($this->request);

		$this->leaveType = $leaveType;
	}

	/**
	 * Private method to generate or get the weekdays duration for the request
	 * 
	 * @return int number of working days
	 */
	protected function getWeekDaysDuration()
	{
	    return $this->weekDaysDuration;
	}

	public function check(User $user)
	{
		if($this->leaveType->max_number === 0) {
			return true;
		}

		return ($user->leavesCount($this->leaveType) - $this->getWeekDaysDuration()) >= 0 ;
	}
}