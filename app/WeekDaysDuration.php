<?php

namespace Koala;

use Carbon\Carbon;

class WeekDaysDuration
{
    public static function get($request)
    {
        $result = Carbon::parse($request->leave_end)->diffInDaysFiltered(function(Carbon $date) {
            return ! $date->isWeekend();
        }, Carbon::parse($request->leave_start));

        return Carbon::parse($request->leave_end)->isWeekend() ? $result : $result+1 ;
    }

}
