<?php

namespace Koala\Http\Middleware;

use Closure;

class CheckIfIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->type !== 'admin') {
            flash("You can't access this page.");
            return redirect(route('home'));
        }
        
        return $next($request);
    }
}
