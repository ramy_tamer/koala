<?php

namespace Koala\Http\Controllers;

use Socialite;
use Koala\User;
use Illuminate\Http\Request;

class OAuthController extends Controller
{
    protected $guzzle;

    public function __construct(\GuzzleHttp\Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }
    /**
     * @codeCoverageIgnore
     */
    public function login($provider = 'google')
    {
    	return Socialite::with($provider)->redirect();
    }

    public function callback(Request $request, $provider = 'google')
    {
    	if(! $request->has('code')) {
    		flash('You Canceled !', 'danger');
    		return redirect(route('welcome'));
    	}

    	$user = Socialite::with($provider)->user();

    	if(! strpos($user->email, '@accorpa')) {
    		return $this->deauth($provider, $user->token);
    	}

    	return $this->storeUser($user);
    }

    public function storeUser($googleUser)
    {
    	$user = User::updateOrCreate([
    		'email' => $googleUser->email
    	], [
			'name'         => $googleUser->name,
			'avatar'       => $googleUser->avatar_original,
			'google_id'    => $googleUser->id,
			'google_token' => $googleUser->token
    	]);

    	auth()->login($user);

    	flash('Welcome to Koala !', 'success');
    	return redirect(session()->pull('url.intended', route('home')));
    }

    public function deauth($provider = 'google', $token)
    {
        $this->guzzle->get("https://accounts.google.com/o/oauth2/revoke?token={$token}");

    	flash('You must login with your accorpa account [@accorpa.com].', 'warning');

    	return redirect(route('welcome'));
    }
}
