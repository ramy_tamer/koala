<?php

namespace Koala\Http\Controllers;

use Koala\LeaveType;
use Illuminate\Http\Request;

class LeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leaveTypes = LeaveType::orderBy('id')->get();
        
        return view('leaveTypes.index')->with(compact('leaveTypes'));
    }
}
