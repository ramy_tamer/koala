<?php

namespace Koala\Http\Controllers;

use Carbon\Carbon;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Koala\WeekDaysDuration;
use Illuminate\Http\Request;
use Koala\Events\LeaveRequestCreated;
use Koala\Events\LeaveRequestAccepted;
use Koala\Events\LeaveRequestRejected;
use Illuminate\Support\Facades\Validator;
use Koala\Http\Requests\CreateLeaveRequest;

class LeaveRequestController extends Controller
{

    public function index()
    {
        if(auth()->user()->cannot('list', LeaveRequest::class)) {
            flash("You can't view leave requests.", 'danger')->important();
            return redirect(route('home'));
        }

        $leaveRequests = LeaveRequest::requests()->get();

        return view('leaveRequests.index')->with(compact('leaveRequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leaveRequest = new LeaveRequest;

        return view('leaveRequests.create')->with(compact('leaveRequest'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLeaveRequest $request)
    {
        $weekDaysDuration = WeekDaysDuration::get($request);

        $leaveType = LeaveType::find($request->leave_type_id);

        if(auth()->user()->cannot('create', LeaveRequest::class)) {
            $days = str_plural('day', $weekDaysDuration);
            flash("You can't request {$weekDaysDuration} {$days} of {$leaveType->title} leave.", 'danger')->important();
            return back()->withInput();
        }


        $leaveRequest = LeaveRequest::create($request->all() + [
            'employee_id' => auth()->user()->id,
            'status'      => ($leaveType->auto_accept || auth()->user()->type === 'admin') ? 'accepted' : 'reviewing' 
        ]);
        
        event(new LeaveRequestCreated($leaveRequest));

        return redirect(route('leave-request.show', $leaveRequest));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveRequest $leaveRequest)
    {
        if(auth()->user()->cannot('view', $leaveRequest)) {
            return redirect(route('home'));
        }

        $leaveRequest->load('employee', 'admin', 'messages.sender');

        return view('leaveRequests.show')->with(compact('leaveRequest'));
    }

    /**
     * Accept the leave request
     * 
     * @param  LeaveRequest $leaveRequest
     * @return Illuminate\Http\Response                     
     */
    public function accept(Request $request, LeaveRequest $leaveRequest)
    {
        if(auth()->user()->cannot('respond', $leaveRequest)) {
            flash("You can't accept this leave request.", 'danger')->important();
            return redirect(route('home'));
        }

        $leaveRequest->update([
            'status' => 'accepted',
            'admin_id' => auth()->user()->id
        ]);

        event(new LeaveRequestAccepted($leaveRequest));

        return redirect(route('leave-request.show', $leaveRequest->id));
    }

    /**
     * Reject the leave request
     * 
     * @param  LeaveRequest $leaveRequest
     * @return Illuminate\Http\Response                     
     */
    public function reject(Request $request, LeaveRequest $leaveRequest)
    {
        if(auth()->user()->cannot('respond', $leaveRequest)) {
            flash("You can't reject this leave request.", 'danger')->important();
            return redirect(route('home'));
        }

        $leaveRequest->update([
            'status' => 'rejected',
            'admin_id' => auth()->id()
        ]);

        event(new LeaveRequestRejected($leaveRequest));

        return redirect(route('leave-request.show', $leaveRequest->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Koala\LeaveRequest  $leaveRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveRequest $leaveRequest)
    {
        if(auth()->user()->cannot('delete', $leaveRequest)) {
            flash("You can't cancel this leave request.", 'danger')->important();
            return redirect(route('home'));
        }

        $leaveRequest->delete();
        return redirect(route('home'));
    }
}
