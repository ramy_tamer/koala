<?php

namespace Koala\Http\Controllers;

use Koala\User;
use Koala\Message;
use Koala\LeaveRequest;
use Illuminate\Http\Request;
use Koala\Notifications\MessageSent;
use Koala\Http\Requests\SendMessageRequest;
use Illuminate\Support\Facades\Notification;

class MessageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SendMessageRequest $request)
    {
        if(auth()->user()->cannot('create', Message::class)) {
            flash("You can't send a message on this thread.", 'danger')->important();
            return redirect(route('home'));
        }

        $message = Message::create([
            'sender_id'        => auth()->id(),
            'leave_request_id' => $request->leave_request_id,
            'message'          => $request->message
        ]);

        if(auth()->user()->type === 'admin') {
            $message->leaveRequest->employee->notify(new MessageSent($message));
        } else {
            $admins = User::where('type', 'admin')->get();
            Notification::send($admins, new MessageSent($message));
        }

        return redirect(route('leave-request.show', $request->leave_request_id));
    }
}
