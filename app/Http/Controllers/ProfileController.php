<?php

namespace Koala\Http\Controllers;

use Koala\User;
use Koala\LeaveType;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
	public function __invoke(User $user)
	{
		if(auth()->user()->cannot('view-profile', $user)) {
			flash("You can't view other employees profiles.", 'danger')->important();
			return redirect(route('home'));
		}

		$user = $user->load('leaveRequests.type');

		$normalLeaves = $user->normal_leaves;
		$casualLeaves = $user->casual_leaves;

		$leaveTypes = cache('leaveTypes');

		$leavesCount = [];

		foreach ($leaveTypes as $type) {
			$leavesCount[] = [
				'type'			=> $type->title,
				'count'			=> abs($type->max_number - $user->leavesCount($type))
			];
		}

		// dd(\Koala\LeaveRequest::all()->toArray(), $user->toArray());

		return view('profile')->with(compact('user', 'leavesCount'));
	}
}
