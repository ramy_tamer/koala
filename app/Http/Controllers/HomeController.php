<?php

namespace Koala\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
   public function __invoke()
   {
		$currentUser = auth()->user()->load('leaveRequests.admin', 'leaveRequests.type');

		$normalLeaves = $currentUser->normal_leaves;
		$casualLeaves = $currentUser->casual_leaves;
		$unpaidLeaves = $currentUser->unpaid_leaves;

	    return view('home')->with(compact('currentUser', 'normalLeaves', 'casualLeaves', 'unpaidLeaves'));
   }
}
