<?php

namespace Koala\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'          => ['required', 'min:3', 'max:4000'],
            'leave_request_id' => ['required', 'exists:leave_requests,id'],
        ];
    }
}
