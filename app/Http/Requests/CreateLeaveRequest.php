<?php

namespace Koala\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateLeaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'leave_type_id' => ['required', 'exists:leave_types,id'],
            'reason'        => ['string', 'min:5', 'max:6000'],
            'leave_start'   => ['required', 'date', 'after:yesterday'],
            'leave_end'     => ['required', 'date', "after_or_equal:leave_start"],
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     * 
     * @return array 
     */
    public function messages()
    {
        return [
            'after_or_equal' => "The leave end must be a date after or equal leave start."
        ];
    }
}
