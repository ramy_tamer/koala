<?php

namespace Koala;

use Koala\User;
use Carbon\Carbon;
use Koala\Message;
use Koala\LeaveType;
use Koala\WeekDaysDuration;
use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{

	protected $dates = ['leave_start', 'leave_end'];

    protected $fillable = ['employee_id', 'leave_type_id', 'reason', 'leave_start', 'leave_end', 'status', 'admin_id'];

    // protected $with = ['employee', 'admin', 'type'];

    public function type()
    {
        return $this->belongsTo(LeaveType::class, 'leave_type_id', 'id');
    }

    public function scopeIs($query, $type)
    {
        $leaveTypeId = ($type instanceof LeaveType) ? $type->id : $type;  

        return $query->where('leave_type_id', $leaveTypeId);
    }

    public function employee()
    {
    	return $this->belongsTo(User::class, 'employee_id');
    }

    public function admin()
    {
    	return $this->belongsTo(User::class, 'admin_id');
    }

    public function messages()
    {
    	return $this->hasMany(Message::class);
    }

    public function getDurationAttribute()
    {
        return WeekDaysDuration::get($this);
    }

    public function scopeRequests($query)
    {
        return $query->where('status', 'reviewing');
    }

    public function scopeAccepted($query)
    {
        return $query->where('status', 'accepted');
    }
}
