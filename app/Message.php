<?php

namespace Koala;

use Koala\User;
use Koala\LeaveRequest;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

	protected $fillable = ['sender_id', 'leave_request_id', 'message'];

    public function leaveRequest()
    {
    	return $this->belongsTo(LeaveRequest::class);
    }

    public function sender()
    {
    	return $this->belongsTo(User::class, 'sender_id');
    }
}
