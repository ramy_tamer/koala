const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

let paths = {
	jquery: './bower_components/jquery',
	simplex: './bower_components/bootswatch-dist',
  social: './bower_components/bootstrap-social',
  fontawesome: './bower_components/font-awesome',
  bootstrapselect: './bower_components/bootstrap-select',
  bootstrapdatepicker: './bower_components/bootstrap-datepicker',
  jqueryujs: './bower_components/jquery-ujs',
};

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
  mix
  	.webpack('app.js')
    .copy(`${paths.simplex}/fonts/`, 'public/fonts/')
  	.copy(`${paths.fontawesome}/fonts/`, 'public/fonts/')
  	.styles([
      `${paths.simplex}/css/bootstrap.css`,
      `${paths.fontawesome}/css/font-awesome.css`,
      `${paths.social}/bootstrap-social.css`,
      `${paths.bootstrapselect}/dist/css/bootstrap-select.css`,
      `${paths.bootstrapdatepicker}/dist/css/bootstrap-datepicker.css`,
      'main.css',
    ])
    .scripts([
      `${paths.simplex}/js/bootstrap.js`,
      `${paths.bootstrapselect}/dist/js/bootstrap-select.js`,
      `${paths.bootstrapdatepicker}/dist/js/bootstrap-datepicker.js`,
  		`${paths.jqueryujs}/src/rails.js`,
    ])
    ;
});
