@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">
                {{ $user->name }}'s Leave Requests Log
            </h3>

            <hr>

            <div class="row">

                <div class="col-xs-12 col-md-4 col-md-offset-4">
                    <table class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center">Leave Type</th>
                                <th class="text-center">Total Actual Days</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($leavesCount as $leave)
                                <tr>
                                    <td>{{ $leave['type'] }}</td>
                                    <td>
                                        <strong>{{ $leave['count'] }}</strong>
                                        {{ str_plural('Day', $leave['count']) }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            
            <hr>

            @if($user->leaveRequests->count())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Leave Type</th>
                            <th class="text-center">Leave Status</th>
                            <th class="text-center">Leave Reason</th>
                            <th class="text-center">From</th>
                            <th class="text-center">Till</th>
                            <th class="text-center">Request Date Time</th>
                            <th class="text-center">More Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user->leaveRequests as $leaveRequest)
                            <tr class="text-center {{ $leaveRequest->type === 'normal' ? 'normal-row' : 'casual-row' }}">
                                <td>{{ $loop->remaining + 1 }}</td>
                                <td>{{ ucwords($leaveRequest->type) }}</td>
                                <td>
                                    <span
                                        class="label label-{{ $leaveRequest->status === 'accepted' ? 'success' : ( $leaveRequest->status === 'rejected' ? 'default' : 'info' ) }}">
                                        {{ ucwords($leaveRequest->status) }}
                                    </span>
                                </td>
                                <td>{{ str_limit( nl2br(e($leaveRequest->reason)) , 40) }}</td>
                                <td>{{ $leaveRequest->leave_start->toDateString() }}</td>
                                <td>
                                    {{ $leaveRequest->leave_end->toDateString() }}
                                    [{{ $leaveRequest->duration }} {{ str_plural('day', $leaveRequest->duration) }}]
                                </td>
                                <td title="{{ $leaveRequest->created_at }}">{{ $leaveRequest->created_at->diffForHumans() }}</td>
                                <td>
                                    <a class="btn btn-sm btn-default" href="{{ route('leave-request.show', $leaveRequest) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h1 class="text-info text-center">
                    @if (auth()->id() === $user->id)
                        You don't have any logs right now.
                    @else
                        {{ $user->name }} doesn't have any logs right now.
                    @endif
                </h1>
            @endif
        </div> 
    </div>
@endsection