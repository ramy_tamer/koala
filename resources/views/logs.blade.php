@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1 class="text-center">All Users</h1>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">id</th>
                        <th class="text-center">Avatar</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">View profile</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr class="text-center">
                            <td>{{ $user->id }}</td>
                            <td>
                                <img width="50" src="{{ $user->avatar }}">
                            </td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ ucwords($user->type) }}</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('profile', $user->id) }}">
                                    <i class="fa fa-user"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div> 
    </div>
@endsection