<div class="col-xs-12 col-md-4">
	<h3 class="text-center text-danger">
		{{ $unpaidLeaves }} Total Unpaid Leave {{ str_plural('Day', $unpaidLeaves) }}
	</h3>
</div>