@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a class="btn btn-default" href="{{ route('leave-type.index') }}">Manage Leave Types</a>
            <a class="btn btn-default" href="{{ route('logs') }}">Users' Logs</a>
        </div>
    </div>
@endsection