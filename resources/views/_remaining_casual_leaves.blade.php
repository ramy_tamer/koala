<div class="col-xs-12 col-md-4">
    <label>Remaining Casual Leaves [ <b>{{ $casualLeaves }}</b> / 7 ]</label>
    <div class="progress">
      <div
        class="progress-bar progress-bar-info"
        role="progressbar"
        aria-valuenow="{{ round( ($casualLeaves / 7.0)*100 ) }}"
        aria-valuemin="0"
        aria-valuemax="100"
        style="width: {{ round( ($casualLeaves / 7.0)*100 ) }}%;">
        {{ round( ($casualLeaves / 7.0)*100 ) }}%
      </div>
    </div>
</div>