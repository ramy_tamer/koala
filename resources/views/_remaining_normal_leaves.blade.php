<div class="col-xs-12 col-md-4">
    <label>Remaining Normal Leaves [ <b>{{ $normalLeaves }}</b> / 14 ]</label>
    <div class="progress">
      <div
        class="progress-bar progress-bar-success"
        role="progressbar"
        aria-valuenow="{{ round( ($normalLeaves / 14.0) * 100 ) }}"
        aria-valuemin="0"
        aria-valuemax="100"
        style="width: {{ round( ($normalLeaves / 14.0) * 100 ) }}%;">
        {{ round( ($normalLeaves / 14.0) * 100 ) }}%
      </div>
    </div>
</div>