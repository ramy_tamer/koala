@extends('layouts.main')

@section('content')
    <div class="row">
        
        @include('_remaining_normal_leaves', ['user' => $currentUser, 'normalLeaves' => $normalLeaves])
        @include('_remaining_casual_leaves', ['user' => $currentUser, 'casualLeaves' => $casualLeaves])
        @include('_accepted_unpaid_leaves', ['user' => $currentUser, 'unpaidLeaves' => $unpaidLeaves])

        <div class="col-xs-12">
            <a href="{{ route('leave-request.create') }}" class="btn btn-warning" id="request-leave">
                <i class="fa fa-plus"></i> Request Leave
            </a>
        </div>
    </div>
    <hr>
    <div class="row">
    	<div class="col-xs-12">
    		<h3>Your History :</h3>
    		<br>
    		@if($currentUser->leaveRequests->count())
    			<table class="table table-bordered">
    				<thead>
    					<tr>
    						<th class="text-center">#</th>
    						<th class="text-center">Leave Type</th>
    						<th class="text-center">Leave Reason</th>
    						<th class="text-center">From</th>
    						<th class="text-center">Till</th>
    						<th class="text-center">Request Date Time</th>
    						<th class="text-center">Request Status</th>
    						<th class="text-center">Responsable Admin</th>
    						<th class="text-center">More Info</th>
    					</tr>
    				</thead>
    				<tbody>
    					@foreach($currentUser->leaveRequests->reverse() as $leaveRequest)
    						<tr class="text-center {{ $leaveRequest->type->auto_accept ? 'normal-row' : 'casual-row' }}">
    							<td>{{ $loop->remaining+1 }}</td>
    							<td>{{ ucwords($leaveRequest->type) }}</td>
    							<td>{{ str_limit( nl2br(e($leaveRequest->reason)) , 40) }}</td>
    							<td>{{ $leaveRequest->leave_start->toDateString() }}</td>
    							<td>
    								{{ $leaveRequest->leave_end->toDateString() }}
                                    [{{ $leaveRequest->duration }} {{ str_plural('day', $leaveRequest->duration) }}]
    							</td>
    							<td title="{{ $leaveRequest->created_at }}">{{ $leaveRequest->created_at->diffForHumans() }}</td>
    							<td>
    								<span
    									class="label label-{{ $leaveRequest->status === 'accepted' ? 'success' : ( $leaveRequest->status === 'rejected' ? 'default' : 'info' ) }}">
    									{{ ucwords($leaveRequest->status) }}
    								</span>
    							</td>
    							<td>
    								@if($leaveRequest->admin)
    									{{ $leaveRequest->admin->name }}
    								@else
    									-
    								@endif
    							</td>
    							<td>
    								<a class="btn btn-sm btn-default" href="{{ route('leave-request.show', $leaveRequest) }}">
    									<i class="fa fa-eye"></i>
    								</a>
    							</td>
    						</tr>
    					@endforeach
    				</tbody>
    			</table>
    		@else
    			<h1 class="text-info text-center">You don't have any logs right now.</h1>
    		@endif
    	</div>
    </div>
@endsection
