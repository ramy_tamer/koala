<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Koala | Accorpa</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="csrf-param" content="_token" />

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}">

		<script>
		    window.Koala = <?php echo json_encode([
		        'csrfToken' => csrf_token(),
		    ]); ?>
		</script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		@include('layouts.navbar')

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6 col-md-offset-3">
					@include('flash::message')
				</div>	
			</div>

			@yield('content')
		</div>

		<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
		<script type="text/javascript">
			$('div.alert').not('.alert-important').delay(7000).fadeOut('slow');
			$('.selectpicker').selectpicker({
			  style: 'btn-primary',
			  size: 7
			});
			$('.datepicker').datepicker({
				format: "yyyy-mm-dd",
			    startDate: "today",
			    todayBtn: "linked",
			    clearBtn: true,
			    daysOfWeekDisabled: [5, 6]
			});
		</script>
	</body>
</html>