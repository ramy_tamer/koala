@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <h1 class="text-center">Koala HR System</h1>
            <br>
            <img style="display: block; margin: 0 auto; max-width: 200px" src="{{ asset('images/logo.png') }}">
            <br>
            <a href="{{ route('oauth.login', 'google') }}" class="btn btn-block btn-social btn-google" id="btn-google">
              <span class="fa fa-google"></span>
              Sign in with your Accorpa Gmail Account
            </a>
        </div>
    </div>
@endsection
