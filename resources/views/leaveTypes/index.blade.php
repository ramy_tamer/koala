@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1 class="text-center">
                Leave Types

                <a href="" class="btn btn-sm btn-primary">Add <i class="fa fa-plus"></i></a>
            </h1>

            <hr>
            
            @if($leaveTypes->count())
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <th class="text-center">Title</th>
                            <th class="text-center">Is Paid</th>
                            <th class="text-center">Auto Accept</th>
                            <th class="text-center">Max Number</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($leaveTypes as $leaveType)
                            <tr class="text-center">
                                <td>{{ $leaveType->title }}</td>
                                <td>
                                    <i class="fa fa-{{ $leaveType->is_paid ? 'check' : 'times' }}"></i>
                                </td>
                                <td>
                                    <i class="fa fa-{{ $leaveType->auto_accept ? 'check' : 'times' }}"></i>
                                </td>
                                <td>{{ $leaveType->max_number === 0 ? 'No Limit' : $leaveType->max_number }}</td>
                                <td>
                                    <a href="" class="btn btn-primary">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h1 class="text-info text-center">There is no leave types right now.</h1>
            @endif
        </div> 
    </div>
@endsection