@extends('layouts.main')

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="media">
			  <div class="media-left media-top">
			    <a href="{{ route('profile', $leaveRequest->employee) }}">
			      <img
			      	class="media-object"
			      	src="{{ $leaveRequest->employee->avatar }}"
			      	alt="{{ $leaveRequest->employee->name }}"
			      	width="60">
			    </a>
			  </div>
			  <div class="media-body">
			    <h4 class="media-heading">
			    	{{ $leaveRequest->employee->name }}
			    </h4>
			    Normal Leaves: <strong>{{ $leaveRequest->employee->normal_leaves }}</strong> / 14
			    <br>
			    Casual Leaves: <strong>{{ $leaveRequest->employee->casual_leaves }}</strong> / 7	
			  </div>
			</div>

			<br>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">
								{{ ucwords($leaveRequest->type->title) }} Leave Request - [{{ $leaveRequest->type->is_paid ? 'Paid' : 'Unpaid' }}]

								<span
									class="pull-right label label-{{ $leaveRequest->status === 'accepted' ? 'success' : ( $leaveRequest->status === 'rejected' ? 'default' : 'info' ) }}">
									{{ ucwords($leaveRequest->status) }}
								</span>
							</h3>
						</div>
						<div class="panel-body">
							From: <strong>{{ $leaveRequest->leave_start->toDateString() }}</strong>
							<span class="pull-right">
								Till: 
								<strong>
									{{ $leaveRequest->leave_end->toDateString() }}
									[{{ $leaveRequest->duration }} {{ str_plural('day', $leaveRequest->duration) }}]
								</strong>
							</span>
							<hr>
							<p>
								{!! nl2br(e($leaveRequest->reason)) !!}
							</p>
						</div>
						<div class="panel-footer">
							@can('delete', $leaveRequest)
								<a
									class="btn btn-primary pull-right"
									href="{{ route('leave-request.destroy', $leaveRequest) }}"
									data-method="delete"
									data-confirm="Are you sure you want to cancel this leave request ?"
									id="cancel-request"
									>
									<i class="fa fa-trash"></i> Cancel Request
								</a>
							@endcan
							Requested At:
							<strong>{{ $leaveRequest->created_at->toDateString() }} [{{ $leaveRequest->created_at->diffForHumans() }}]</strong>
							<br><br>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
				@foreach($leaveRequest->messages as $message)
					<div class="media">
					  <div class="media-left media-top">
					    <a href="">
					      <img
					      	class="media-object"
					      	src="{{ $message->sender->avatar }}"
					      	alt="{{ $message->sender->name }}"
					      	width="60">
					    </a>
					  </div>
					  <div class="media-body">
					    <div class="panel panel-sm {{ $message->sender->type === 'admin' ? 'panel-warning' : 'panel-info' }}">
					    	<div class="panel-heading">
					    		<h3 class="panel-title">
					    			{{ $message->sender->name }}
					    			<small class="text-muted pull-right" title="{{ $message->created_at->toDateString() }}">{{ $message->created_at->diffForHumans() }}</small>
					    		</h3>
					    	</div>
					    	<div class="panel-body">
					    		{!! nl2br(e($message->message)) !!}
					    	</div>
					    </div>							    
					  </div>
					</div>
				@endforeach


				@if($leaveRequest->status === 'reviewing')
					
					@can('respond', $leaveRequest)
						{!! Form::open([
							'route' => ['leave-request.accept', $leaveRequest->id],
							'method' => 'PUT',
							'style' => 'display: inline'
						]) !!}
							<button type="submit" class="btn btn-info">Accept Request</button>
						{!! Form::close() !!}

						{!! Form::open([
							'route' => ['leave-request.reject', $leaveRequest->id],
							'method' => 'DELETE',
							'style' => 'display: inline'
						]) !!}
							<button type="submit" class="btn btn-danger">Reject Request</button>
						{!! Form::close() !!}
					@endcan

					
					<div class="media">
					  @include('layouts.errors')

					  <div class="media-left media-top">
					    <a>
					      <img
					      	class="media-object"
					      	src="{{ auth()->user()->avatar }}"
					      	alt="{{ auth()->user()->name }}"
					      	width="60">
					    </a>
					  </div>
					  <div class="media-body">
					    <h4 class="media-heading">
					    	{!! Form::open(['route' => 'message.store', 'method' => 'POST']) !!}
					    		<div class="form-group">
					    			{!! Form::hidden('leave_request_id', $leaveRequest->id) !!}
					    			{!! Form::textarea('message', old('message'), ['class' => 'form-control', 'rows' => 4, 'placeholder' => 'The Message']) !!}
					    			<br>
					    			<button class="btn btn-default" type="submit" id="send-message">
					    				<i class="fa fa-envelope-o"></i> Send
					    			</button>
					    		</div>
					    	{!! Form::close() !!}
					    </h4>							    
					  </div>
					</div>
			
				@else
					<h3 class="text-center text-danger">
						This Request is <b>{{ ucwords($leaveRequest->status) }}</b>, you can't send any message to the admin regarding this request.
					</h3>
				@endif
				</div>
			</div>
		</div> 
	</div>
@endsection