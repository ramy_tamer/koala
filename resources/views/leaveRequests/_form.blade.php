<div class="form-group">
	{!! Form::label('type', 'Leave Type', ['class' => 'control-label']) !!}
	<br>
	{!! Form::select('leave_type_id', $leaveTypes , $leaveRequest->leave_type_id, ['class' => 'selectpicker']) !!}
</div>

<div class="form-group">
	{!! Form::label('reason', 'Reason', ['class' => 'control-label']) !!}
	<small class="text-muted">[optional]</small>
	{!! Form::textarea('reason', $leaveRequest->reason, ['class' => 'form-control', 'rows' => 3]) !!}
</div>

<div class="form-group">
	{!! Form::label('leave_start', $leaveRequest->leave_start, ['class' => 'control-label']) !!}
	{!! Form::input(
		'text',
		'leave_start',
		$leaveRequest->leave_start ?? (Carbon::now()->isWeekend() ? Carbon::now()->next(Carbon::SUNDAY) : Carbon::now())->toDateString(),
		['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
	{!! Form::label('leave_end', $leaveRequest->leave_end, ['class' => 'control-label']) !!}
	{!! Form::input(
		'text',
		'leave_end',
		$leaveRequest->leave_end ?? (Carbon::now()->isWeekend() ? Carbon::now()->next(Carbon::SUNDAY) : Carbon::now())->toDateString(),
		['class' => 'form-control datepicker']) !!}
</div>
