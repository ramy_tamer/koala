@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1 class="text-center">Requests waiting to be reviewed</h1>

            @if($leaveRequests->count())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Employee</th>
                            <th class="text-center">Leave Type</th>
                            <th class="text-center">Leave Reason</th>
                            <th class="text-center">From</th>
                            <th class="text-center">Till</th>
                            <th class="text-center">Request Date Time</th>
                            <th class="text-center">More Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($leaveRequests as $leaveRequest)
                            <tr class="text-center {{ $leaveRequest->type === 'normal' ? 'normal-row' : 'casual-row' }}">
                                <td>{{ $loop->index + 1 }}</td>
                                <td>
                                    <a href="{{ route('profile', $leaveRequest->employee->id) }}">
                                        {{ $leaveRequest->employee->name }}
                                    </a>
                                </td>
                                <td>{{ ucwords($leaveRequest->type) }}</td>
                                <td>{{ str_limit( nl2br(e($leaveRequest->reason)) , 40) }}</td>
                                <td>{{ $leaveRequest->leave_start->toDateString() }}</td>
                                <td>
                                    {{ $leaveRequest->leave_end->toDateString() }}
                                    [{{ $leaveRequest->duration }} {{ str_plural('day', $leaveRequest->duration) }}]
                                </td>
                                <td title="{{ $leaveRequest->created_at }}">{{ $leaveRequest->created_at->diffForHumans() }}</td>
                                <td>
                                    <a class="btn btn-sm btn-default" href="{{ route('leave-request.show', $leaveRequest) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h1 class="text-info text-center">You don't have any logs right now.</h1>
            @endif
        </div> 
    </div>
@endsection