@extends('layouts.main')

@section('content')
	
	<div class="row">
		<div class="col-xs-12 col-md-6 col-md-offset-3">
			@include('layouts.errors')
			
			{!! Form::model($leaveRequest, ['route' => 'leave-request.store', 'method' => 'POST']) !!}
				<legend>Request Leave</legend>
				@include('leaveRequests._form')

				<button type="submit" class="btn btn-info btn-block">
					<i class="fa fa-plus"></i> Submit Request
				</button>
			{!! Form::close() !!}
		</div>
	</div>

@endsection