<?php

use Carbon\Carbon;
use Koala\LeaveType;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Koala\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => preg_replace('/@example\..*/', '@accorpa.com', $faker->unique()->safeEmail),
        'type'           => 'employee',
        'password'       => $password ?: $password = bcrypt('secret'),
        'avatar'         => $faker->imageUrl,
        'google_id'      => rand(0000000000000000, 9999999999999999),
        'google_token'   => str_random(31),
        'remember_token' => str_random(10),
        'normal_leaves'  => 14,
        'casual_leaves'  => 7,
        'unpaid_leaves'  => 0,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Koala\LeaveType::class, function (Faker\Generator $faker) {
    return [
        'title'       => $faker->title,
        'is_paid'     => true,
        'auto_accept' => false,
        'max_number'  => 0
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Koala\LeaveRequest::class, function (Faker\Generator $faker) {
    $leave_start = Carbon::now()->next(Carbon::SUNDAY);
    $leave_end   = (clone $leave_start)->addDays(2);

    return [
        'employee_id'   => function() { return factory(Koala\User::class)->create()->id; },
        'leave_type_id' => function() {
            return LeaveType::firstOrCreate(factory(LeaveType::class)->states('normal')->make()->toArray())->id;
        },
        'status'        => 'reviewing',
        'reason'        => $faker->paragraph,
        'leave_start'   => $leave_start->toDateString(),
        'leave_end'     => $leave_end->toDateString(),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Koala\Message::class, function (Faker\Generator $faker) {
    return [
        'leave_request_id' => function() { return factory(Koala\LeaveRequest::class)->create()->id; },
        'sender_id'        => function() { return factory(Koala\User::class)->create()->id; },
        'message'          => $faker->paragraph,
    ];
});

$factory
    ->state(Koala\User::class, 'admin', function($faker) {
        return [
            'type' => 'admin'
        ];
    })
    ->state(Koala\User::class, 'employee', function($faker) {
        return [
            'type' => 'employee'
        ];
    });

$factory
    ->state(Koala\LeaveType::class, 'normal', function($faker) {
        return [ 'title' => 'Normal', 'max_number' => 14 ];
    })
    ->state(Koala\LeaveType::class, 'casual', function($faker) {
        return [ 'title' => 'Casual', 'auto_accept' => true, 'max_number' => 7 ];
    })
    ->state(Koala\LeaveType::class, 'unpaid', function($faker) {
        return [ 'title' => 'Unpaid', 'is_paid' => false ];
    })
    ->state(Koala\LeaveType::class, 'maternity', function($faker) {
        return [ 'title' => 'Maternity' ];
    })
    ->state(Koala\LeaveType::class, 'sick', function($faker) {
        return [ 'title' => 'Sick' ];
    });

$factory
    ->state(Koala\LeaveRequest::class, 'oneDay', function($faker) {
        $leave_start = Carbon::now()->next(Carbon::SUNDAY);

        return [
            'leave_start' => $leave_start->toDateString(),
            'leave_end' => $leave_start->toDateString(),
        ];
    })
    ->state(Koala\LeaveRequest::class, 'casual', function($faker) {
        return [
            'leave_type_id' => function() {
                return LeaveType::firstOrCreate(factory(LeaveType::class)->states('casual')->make()->toArray())->id;
            }
        ];
    })
    ->state(Koala\LeaveRequest::class, 'unpaid', function($faker) {
        return [
            'leave_type_id' => function() {
                return LeaveType::firstOrCreate(factory(LeaveType::class)->states('unpaid')->make()->toArray())->id;
            }
        ];
    })
    ->state(Koala\LeaveRequest::class, 'maternity', function($faker) {
        return [
            'leave_type_id' => function() {
                return LeaveType::firstOrCreate(factory(LeaveType::class)->states('maternity')->make()->toArray())->id;
            }
        ];
    })
    ->state(Koala\LeaveRequest::class, 'sick', function($faker) {
        return [
            'leave_type_id' => function() {
                return LeaveType::firstOrCreate(factory(LeaveType::class)->states('sick')->make()->toArray())->id;
            }
        ];
    })
    ->state(Koala\LeaveRequest::class, 'admin', function($faker) {
        return [
            'admin_id' => function() { return factory(Koala\User::class)->states('admin')->create()->id; }
        ];
    })
    ->state(Koala\LeaveRequest::class, 'rejected', function($faker) {
        return [
            'status' => 'rejected'
        ];
    })
    ->state(Koala\LeaveRequest::class, 'accepted', function($faker) {
        return [
            'status' => 'accepted'
        ];
    });