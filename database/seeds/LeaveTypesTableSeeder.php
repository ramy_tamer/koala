<?php

use Koala\LeaveType;
use Illuminate\Database\Seeder;

class LeaveTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveType::truncate();
        
      	LeaveType::firstOrCreate(['title' => 'Normal', 'is_paid' => true, 'max_number' => 14]);
      	LeaveType::firstOrCreate(['title' => 'Casual', 'is_paid' => true, 'auto_accept' => true, 'max_number' => 7]);
      	LeaveType::firstOrCreate(['title' => 'Unpaid', 'is_paid' => false]);
      	LeaveType::firstOrCreate(['title' => 'Maternity', 'is_paid' => true]);
      	LeaveType::firstOrCreate(['title' => 'Sick', 'is_paid' => true]);
    }
}
