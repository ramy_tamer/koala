<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnpaidToTypeColumnInLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_requests', function (Blueprint $table) {
            DB::statement("ALTER TABLE leave_requests CHANGE COLUMN type type ENUM('casual', 'normal', 'unpaid')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_requests', function (Blueprint $table) {
            DB::statement("ALTER TABLE leave_requests CHANGE COLUMN type type ENUM('casual', 'normal')");
        });
    }
}
