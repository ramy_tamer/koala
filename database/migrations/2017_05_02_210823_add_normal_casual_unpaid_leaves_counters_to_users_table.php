<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNormalCasualUnpaidLeavesCountersToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('unpaid_leaves')->unsigned()->default(0)->after('mobile');
            $table->integer('casual_leaves')->unsigned()->default(7)->after('mobile');
            $table->integer('normal_leaves')->unsigned()->default(14)->after('mobile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('unpaid_leaves', 'casual_leaves', 'normal_leaves');
        });
    }
}
