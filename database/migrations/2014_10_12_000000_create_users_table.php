<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->default(bcrypt(env('DEFAULT_PASSWORD')));
            $table->string('avatar')->nullable();
            $table->string('mobile')->nullable();
            $table->smallInteger('casual_leaves')->unsigned()->default(7);
            $table->smallInteger('normal_leaves')->unsigned()->default(14);
            $table->enum('type', ['admin', 'employee'])->default('employee');
            $table->string('google_id')->unique();
            $table->text('google_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
