<?php

namespace Tests\Features;

use Koala\User;
use Carbon\Carbon;
use Koala\Message;
use Tests\TestCase;
use Koala\LeaveRequest;
use Faker\Factory as Faker;
use Koala\WeekDaysDuration;
use Koala\Notifications\MessageSent;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SendMessageTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function an_employee_can_send_message_on_a_leave_request_thread()
    {
        // given
        $this->signIn($employee = factory(User::class)->create()); 

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest();
        $this->seeRouteIs('leave-request.show', $leaveRequestId);

        $message = factory(Message::class)->make();
        $this->storeInput('message', $message->message)
             ->press('Send');

        // then
        $this->seeRouteIs('leave-request.show', $leaveRequestId)
             ->seeInDatabase('messages', [
                'sender_id' => $employee->id,
                'leave_request_id' => $leaveRequestId,
                'message' => $message->message
            ])
            ->seeText('Send');
    }

    /** @test */
    public function an_employee_cannot_send_an_empty_message_on_a_leave_request_thread()
    {
        // given
        $this->withExceptionHandling()->signIn($employee = factory(User::class)->create()); 

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest();
        $this->seeRouteIs('leave-request.show', $leaveRequestId);

        $message = factory(Message::class)->make(['message' => null]);
        $this->storeInput('message', null)
             ->press('Send');

        // then
        $this->seeRouteIs('leave-request.show', $leaveRequestId)
             ->dontSeeInDatabase('messages', [
                'sender_id' => $employee->id,
                'leave_request_id' => $leaveRequestId,
                'message' => $message->message
            ])
            ->seeText(trans('validation.required', ['attribute' => 'message']));

        // when
        $this->post(route('message.store'), $message->toArray());

        // then
        $this->assertSessionHasErrors(['message']);
    }

    /** @test */
    public function an_employee_cannot_send_message_on_another_employee_leave_request_thread()
    {
        // given
        $this->signIn($employee = factory(User::class)->create()); 

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest();

        $this->signIn($otherEmployee = factory(User::class)->create());

        $message = factory(Message::class)->make();
        $response = $this->post(route('message.store'), [
            'message'          => $message->message,
            'leave_request_id' => $leaveRequestId
        ]);

        $response->followRedirects();

        // then
        $this->seeRouteIs('home')
             ->notSeeInDatabase('messages', [
                'message'          => $message->message,
                'leave_request_id' => $leaveRequestId     
             ])
             ->seeText("You can't send a message on this thread.");
    }

    /** @test */
    public function an_admin_can_reply_to_an_employee_on_leave_request_thread()
    {
        // given
        $this->signIn($employee = factory(User::class)->create()); 
        list($response, $leaveRequestId) = $this->createLeaveRequest();
        $message = factory(Message::class)->create(['leave_request_id' => $leaveRequestId, 'sender_id' => $employee->id]);

        // when
        $this->signIn($admin = factory(User::class)->states('admin')->create());

        $newMessage = factory(Message::class)->make();
        $response = $this->post(route('message.store'), [
            'message'          => $newMessage->message,
            'leave_request_id' => $leaveRequestId
        ]);

        $response->followRedirects();

        // then
        $this->seeRouteIs('leave-request.show', $leaveRequestId)
             ->seeInDatabase('messages', [
                'sender_id' => $admin->id,
                'leave_request_id' => $leaveRequestId,
                'message' => $newMessage->message
            ])
            ->seeText('Send');
    }

    /** @test */
    public function an_employee_cannot_send_message_on_a_leave_request_thread_that_doesnot_exist()
    {
        // given
        $this->withExceptionHandling()->signIn($employee = factory(User::class)->create()); 

        // when
        $message = factory(Message::class)->make();
        $response = $this->post(route('message.store'), [
            'message'          => $message->message,
            'leave_request_id' => 550
        ]);


        // then
        $this->assertSessionHasErrors(['leave_request_id']);
    }
    
    /** @test */
    public function an_employee_get_notification_if_admin_sent_a_message_on_a_leave_request_he_created()
    {
        // given
        Notification::fake();

        $employee = factory(User::class)->create();
        $leaveRequest = factory(LeaveRequest::class)->create(['employee_id' => $employee->id]);
        $admin = factory(User::class)->states('admin')->create();
        $this->signIn($admin);

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id)
             ->storeInput('message', 'hopa lala')
             ->press('Send');

        // then
        Notification::assertSentTo(
            [ $employee ],
            MessageSent::class
        );
    }

    /** @test */
    public function all_admins_get_notification_if_an_employee_sent_a_message_on_a_leave_request_thread()
    {
        // given
        Notification::fake();

        $employee = factory(User::class)->create();
        $leaveRequest = factory(LeaveRequest::class)->create(['employee_id' => $employee->id]);
        $admins = factory(User::class, 3)->states('admin')->create();
        $this->signIn($employee);

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id)
             ->storeInput('message', 'hopa lala')
             ->press('Send');

        // then
        Notification::assertSentTo(
            $admins,
            MessageSent::class
        );
    }
}
