<?php

namespace Tests\Features;

use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveRequest;
use Faker\Factory as Faker;
use Koala\WeekDaysDuration;
use Illuminate\Support\Facades\Notification;
use Koala\Notifications\EmployeeRequestedLeave;
use Koala\Notifications\AdminAcceptedLeaveRequest;
use Koala\Notifications\AdminRejectedLeaveRequest;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReplyToLeaveRequestTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function admin_can_not_cancel_leave_request()
    {
    	// given
    	$this->signIn(factory(User::class)->states('admin')->create());
    	$leaveRequest = factory(LeaveRequest::class)->create();

    	// when
    	$this->visitRoute('leave-request.show', $leaveRequest);

    	// then
    	$this->dontSee('Cancel Request');

    	// when
    	$this->delete(route('leave-request.destroy', $leaveRequest), ['_token' => csrf_token()])
             ->followRedirects();

        // then
        $this->seeRouteIs('home')
        	 ->seeText("You can't cancel this leave request.");
    }

    /** @test */
    public function admin_can_approve_leave_request()
    {
        // given
        $this->signIn($employee = factory(User::class)->create());

        $leaveRequest = factory(LeaveRequest::class)->create(['employee_id' => $employee->id]);

        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-request.show', $leaveRequest)
             ->press('Accept Request');

        // then
        $this->seeRouteIs('leave-request.show', $leaveRequest);

        $employee = $employee->fresh();

        $this->assertEquals(7, $employee->casual_leaves);
        $this->assertEquals(14 - WeekDaysDuration::get($leaveRequest), $employee->normal_leaves);

        $this->seeInDatabase('leave_requests', [
                'id' => $leaveRequest->id,
                'admin_id' => $admin->id,
                'status' => 'accepted'
             ])
            ->seeText("This Request is Accepted, you can't send any message to the admin regarding this request.");
        
    }

    /** @test */
    public function admin_can_reject_leave_request()
    {
        // given
        $this->signIn();

        list($response, $leaveRequestId) = $this->createLeaveRequest();

        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-request.show', $leaveRequestId)
             ->press('Reject Request');

        // then
        $this->seeRouteIs('leave-request.show', $leaveRequestId)
             ->seeText("This Request is Rejected, you can't send any message to the admin regarding this request.");
    }

    /** @test */
    public function admin_must_reply_to_an_existing_leave_request()
    {
        // given
        $this->withExceptionHandling()
             ->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->put(route('leave-request.accept', 546));

        // then
        $this->assertResponseStatus(404);

        // when
        $this->delete(route('leave-request.reject', 546));

        // then
        $this->assertResponseStatus(404);
    }

    /** @test */
    public function admin_must_reply_to_a_reviewing_leave_request()
    {
        // given
        $this->withExceptionHandling()
             ->signIn($admin = factory(User::class)->states('admin')->create());

        $leaveRequest = factory(LeaveRequest::class)->states('rejected')->create();

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id);

        // then
        $this->dontSeeText('Accept Request')->dontSeeText('Reject Request');

        // when
        $this->put(route('leave-request.accept', $leaveRequest->id))->followRedirects();

        // then
        $this->seeRouteIs('home')
            ->seeInDatabase('leave_requests', [
                'id'     => $leaveRequest->id,
                'status' => 'rejected'
            ])
             ->notSeeInDatabase('leave_requests', [
                'id'     => $leaveRequest->id,
                'status' => 'accepted'
            ])
             ->seeText("You can't accept this leave request.");

        // when
        $this->delete(route('leave-request.reject', $leaveRequest->id))->followRedirects();

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't reject this leave request.");
    }

    /** @test */
    public function only_admin_can_reply_to_a_leave_request()
    {
        // given
        $this->signIn();
        $leaveRequest = factory(LeaveRequest::class)->create();

        // when
        $this->put(route('leave-request.accept', $leaveRequest->id))->followRedirects();

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't accept this leave request.");

        // when
        $this->delete(route('leave-request.reject', $leaveRequest->id))->followRedirects();

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't reject this leave request.");
    }

    /** @test */
    public function admin_can_approve_unpaid_leave_request()
    {
        // given
        $this->signIn($employee = factory(User::class)->create());

        $leaveRequest = factory(LeaveRequest::class)->states('unpaid')->create(['employee_id' => $employee->id]);

        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-request.show', $leaveRequest)
             ->press('Accept Request');

        // then
        $this->assertEquals(7, $employee->casual_leaves);
        $this->assertEquals(14, $employee->normal_leaves);
        $this->assertEquals(WeekDaysDuration::get($leaveRequest), $employee->leavesCount($this->unpaidType));

        $this->seeRouteIs('leave-request.show', $leaveRequest)
             ->seeText("This Request is Accepted, you can't send any message to the admin regarding this request.");
        
    }

    /** @test */
    public function admin_can_reject_unpaid_leave_request()
    {
        // given
        $this->signIn($employee = factory(User::class)->create());

        $leaveRequest = factory(LeaveRequest::class)->states('unpaid')->create(['employee_id' => $employee->id]);

        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-request.show', $leaveRequest)
             ->press('Reject Request');

        // then
        $this->assertEquals(7, $employee->casual_leaves);
        $this->assertEquals(14, $employee->normal_leaves);
        $this->assertEquals(0, $employee->leavesCount($this->unpaidType));

        $this->seeRouteIs('leave-request.show', $leaveRequest)
             ->seeText("This Request is Rejected, you can't send any message to the admin regarding this request.");
        
    }

    /** @test */
    public function a_notification_must_be_sent_to_the_employee_when_his_leave_request_got_approved_by_admin()
    {
        // given
        Notification::fake();

        $employee = factory(User::class)->create();
        $leaveRequest = factory(LeaveRequest::class)->create(['employee_id' => $employee->id]);
        $admin = factory(User::class)->states('admin')->create();
        $this->signIn($admin);

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id)
             ->press('Accept Request');

        // then
        Notification::assertSentTo(
            [ $employee ],
            AdminAcceptedLeaveRequest::class,
            function($notification, $channels) use ($leaveRequest) {
                return $notification->leaveRequest->id === $leaveRequest->id;
            }
        );
    }

    /** @test */
    public function a_notification_must_be_sent_to_the_employee_when_his_leave_request_got_rejected_by_admin()
    {
        // given
        Notification::fake();

        $employee = factory(User::class)->create();
        $leaveRequest = factory(LeaveRequest::class)->create(['employee_id' => $employee->id]);
        $admin = factory(User::class)->states('admin')->create();
        $this->signIn($admin);

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id)
             ->press('Reject Request');

        // then
        Notification::assertSentTo(
            [ $employee ],
            AdminRejectedLeaveRequest::class,
            function($notification, $channels) use ($leaveRequest) {
                return $notification->leaveRequest->id === $leaveRequest->id;
            }
        );
    }

}
