<?php

namespace Tests\Features;

use \Mockery;
use \Socialite;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserLoginTest extends TestCase
{
    use DatabaseTransactions;

    public function tearDown()
    {
        Mockery::close();
    }

    /** @test */
    public function it_shows_login_button()
    {
        $this->visit('/')
             ->see('Koala HR System')
             ->see('Sign in with your Accorpa Gmail Account');
    }

    /** @test */
    public function it_logins_with_google_perfectly()
    {
        $provider = Mockery::mock(Laravel\Socialite\Contracts\Provider::class);
        $provider->shouldReceive('redirect')->andReturn('Redirected');

        $socialAccount = factory(\Koala\User::class)->create();

        $abstractUser                  = Mockery::mock(Laravel\Socialite\Two\User::class);
        $abstractUser->name            = $socialAccount->name;
        $abstractUser->email           = $socialAccount->email;
        $abstractUser->avatar_original = $socialAccount->avatar;
        $abstractUser->id              = $socialAccount->google_id;
        $abstractUser->token           = $socialAccount->google_token;

        $provider = Mockery::mock(Laravel\Socialite\Contracts\Provider::class);
        $provider->shouldReceive('user')->andReturn($abstractUser);

        Socialite::shouldReceive('with')->with('google')->andReturn($provider);

        $route = route('oauth.callback', ['provider' => 'google', 'code' => 'blabla']);
        $this->visit($route)
             ->seePageIs(route('home'))
             ->see($socialAccount->name)
             ->seeText('14 / 14')
             ->seeText('7 / 7')
             ->seeText('0 Total Unpaid Leave Days');
    }

    /** @test */
    public function it_deauth_user_if_not_accorpa_email_provided()
    {
        $provider = Mockery::mock(Laravel\Socialite\Contracts\Provider::class);
        $provider->shouldReceive('redirect')->andReturn('Redirected');

        $socialAccount = factory(\Koala\User::class)->create();

        $abstractUser                  = Mockery::mock(Laravel\Socialite\Two\User::class);
        $abstractUser->name            = $socialAccount->name;
        $abstractUser->email           = "koko@gmail.com";
        $abstractUser->avatar_original = $socialAccount->avatar;
        $abstractUser->id              = $socialAccount->google_id;
        $abstractUser->token           = $socialAccount->google_token;

        $provider = Mockery::mock(Laravel\Socialite\Contracts\Provider::class);
        $provider->shouldReceive('user')->andReturn($abstractUser);

        Socialite::shouldReceive('with')->with('google')->andReturn($provider);

        $guzzle = Mockery::mock(\GuzzleHttp\Client::class);
        $guzzle->shouldReceive('get')
               ->once()
               ->with("https://accounts.google.com/o/oauth2/revoke?token={$socialAccount->google_token}")
               ->andReturn('OK');

        $this->app->instance(\GuzzleHttp\Client::class, $guzzle);

        $route = route('oauth.callback', ['provider' => 'google', 'code' => 'blabla']);
        $this->visit($route)
             ->seePageIs(route('welcome'))
             ->see('Sign in with your Accorpa Gmail Account')
             ->see('You must login with your accorpa account [@accorpa.com].');
    }

    /** @test */
    public function it_handles_invalid_state_exception()
    {
        // given
        $this->withExceptionHandling();

        // when
        // throw new \Laravel\Socialite\Two\InvalidStateException;
        session()->put('state', 'foo');
        $this->visitRoute('oauth.callback', ['provider' => 'google', 'state' => 'baz', 'code' => 'koko']);

        // then
        // dd(session()->all());
        $this->seeRouteIs('welcome')
             ->seeText('Expired authentication link.');
    }

    /** @test */
    public function it_redirect_the_logged_in_user_to_home_page_when_tries_to_login_again()
    {
        // given
        $this->signIn();

        // when
        $this->visitRoute('oauth.login');

        // then
        $this->seeRouteIs('home');

        // when
        $this->visitRoute('welcome');

        // then
        $this->seeRouteIs('home');
    }

    /** @test */
    public function it_detects_if_user_canceled_login()
    {
        // given
        $this->withExceptionHandling();

        // when
        $this->visitRoute('oauth.callback', ['provider' => 'google']);

        // then
        $this->seeRouteIs('welcome')
             ->seeText('You Canceled !');
    }
}
