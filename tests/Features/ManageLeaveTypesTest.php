<?php

namespace Tests\Features;

use Koala\User;
use Tests\TestCase;
use Koala\LeaveType;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ManageLeaveTypesTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function employee_can_not_view_dashboard_page()
    {
        // given
        $this->signIn();

        // when
        $this->visitRoute('dashboard');

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't access this page.");
    }

    /** @test */
    public function employee_can_not_view_leave_type_dashboard_pages()
    {
        // given
        $this->signIn();

        // when
        $this->visitRoute('leave-type.index');

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't access this page.");
    }

    /** @test */
    public function admin_sees_a_message_if_there_is_no_leave_types()
    {
        // given
        LeaveType::truncate();
        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-type.index');

        // then
        $this->seeText("There is no leave types right now.");

        // Fix this test
        LeaveType::firstOrCreate(['title' => 'Normal', 'is_paid' => true, 'max_number' => 14]);
        LeaveType::firstOrCreate(['title' => 'Casual', 'is_paid' => true, 'auto_accept' => true, 'max_number' => 7]);
        LeaveType::firstOrCreate(['title' => 'Unpaid', 'is_paid' => false]);
        LeaveType::firstOrCreate(['title' => 'Maternity', 'is_paid' => true]);
        LeaveType::firstOrCreate(['title' => 'Sick', 'is_paid' => true]);
    }

    /** @test */
    public function admin_sees_leave_types_in_order_by_id()
    {
        // given
        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-type.index');

        // then
        $this->seeTextInOrder(
            $this->normalType->title,
            $this->casualType->title,
            $this->unpaidType->title,
            $this->maternityType->title,
            $this->sickType->title
        );   
    }

}
