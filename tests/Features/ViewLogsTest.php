<?php

namespace Tests\Features;

use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveRequest;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewLogsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_shows_a_message_in_case_of_no_history()
    {
        // given
        $this->signIn();

        // when
        $this->visitRoute('home');

        // then
        $this->seeText("You don't have any logs right now.");
    }

    /** @test */
    public function it_shows_the_previous_logs_of_an_employee()
    {
        // given
        $this->signIn($employee = factory(User::class)->create());
        $accepted  = factory(LeaveRequest::class)->states('accepted')->create(['employee_id' => $employee->id]);
        $rejected  = factory(LeaveRequest::class)->states('rejected')->create(['employee_id' => $employee->id]);
        $reviewing = factory(LeaveRequest::class)->create(['employee_id' => $employee->id]);

        // when
        // $this->visitRoute('home');
        $response = $this->get(route('home'));

        // then
        $this->seeTextInOrder($reviewing->status, $rejected->status, $accepted->status);
        $this->dontSeeTextInOrder($rejected->status, $reviewing->status, $accepted->status);
    }

}
