<?php

namespace Tests\Features;

use \Mockery;
use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Faker\Factory as Faker;
use Koala\WeekDaysDuration;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;
use Koala\Notifications\EmployeeRequestedLeave;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateLeaveRequestTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->leavesTable = (new LeaveRequest)->getTable();

        Mockery::close();
    }

    /** @test */
    public function guest_may_not_request_a_leave()
    {
        // given a non-signed in user
        $this->withExceptionHandling();
        
        // when
        $this->visitRoute('home');

        // then
        $this->seeRouteIs('welcome');

        // when
        $this->visitRoute('leave-request.create');

        // then
        $this->seeRouteIs('welcome')
             ->assertSessionHas('url.intended', route('leave-request.create'));

        // when
        $response = $this->getJson(route('home'))->response;

        // then
        $this->assertEquals(401, $response->getStatusCode());
    }

    /** @test */
    public function an_employee_can_request_a_normal_leave()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        $leaveRequest = factory(LeaveRequest::class)->make();

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        // then
        $this->assertRegExp('/leave-request\/\d+/', $response->currentUri);
        $this->seeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'reviewing',
            'employee_id' => $employee->id,
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ])
        ->seeText('Send')
        ->seeText('Cancel Request');

    }

    /** @test */
    public function an_employee_can_request_a_casual_leave()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        $leaveRequest = factory(LeaveRequest::class)->make();

        // when
        $response = $this->visitRoute('home')
                         ->click('request-leave')
                         ->seeRouteIs('leave-request.create')
                         ->select($this->casualType->id, 'leave_type_id')
                         ->storeInput('reason', $leaveRequest->reason)
                         ->storeInput('leave_start', $leaveRequest->leave_start)
                         ->storeInput('leave_end', $leaveRequest->leave_end)
                         ->press('Submit Request');

        // then
        $this->assertRegExp('/leave-request\/\d+/', $response->currentUri);
        $this->seeInDatabase( $this->leavesTable, [
            'id'          => explode('/', $response->currentUri)[4],
            'status'      => 'accepted',
            'employee_id' => $employee->id,
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ])
        ->seeText("This Request is Accepted, you can't send any message to the admin regarding this request.");

    }

    /** @test */
    public function an_employee_can_cancel_a_normal_leave_request()
    {
        // $this->withoutMiddleware();

        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        $leaveRequest = factory(LeaveRequest::class)->make();

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        // then
        $this->assertRegExp('/leave-request\/\d+/', $response->currentUri);
        
        $this->seeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'reviewing',
            'employee_id' => $employee->id,
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ])
        ->seeText("Send");

        // When
        $this->delete(route('leave-request.destroy', $leaveRequestId), ['_token' => csrf_token()])
             ->followRedirects();
        
        $this->seeRouteIs('home')
             ->seeText("You don't have any logs right now.")
             ->notSeeInDatabase($this->leavesTable, ['id' => $leaveRequestId]);

    }

    /** @test */
    public function an_employee_cannot_cancel_a_non_reviewing_leave_request()
    {
        // given
        $this->signIn($employee = factory(User::class)->create());

        $leaveRequest = factory(LeaveRequest::class)->states('accepted')->create(['employee_id' => $employee->id]);

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id)
             ->dontSeeText('Cancel Request');

        $this->delete(route('leave-request.destroy', $leaveRequest->id))
             ->followRedirects();
        
        // then
        $this->seeRouteIs('home')
             ->seeText("You can't cancel this leave request.")
             ->dontSeeText('Cancel Request');

        $this->visitRoute('leave-request.show', $leaveRequest)
             ->seeText("This Request is {$leaveRequest->status}, you can't send any message to the admin regarding this request.");
    }

    /** @test */
    public function an_employee_cannot_cancel_other_employee_request()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());
        $otherEmployee = factory(User::class)->states('employee')->create();

        $leaveRequest = factory(LeaveRequest::class)->make();

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        $this->signIn($otherEmployee);
        $this->delete(route('leave-request.destroy', $leaveRequestId), ['_token' => csrf_token()])
             ->followRedirects();
        
        // then
        $this->seeRouteIs('home')
             ->seeInDatabase($this->leavesTable, ['id' => $leaveRequestId]);
    }

    /** @test */
    public function an_employee_can_request_one_day_leave()
    {
        // given
        $this->withExceptionHandling()
             ->signIn($employee = factory(User::class)->create());

        $normalLeave = factory(LeaveRequest::class)->states('oneDay')->make();
        $casualLeave = factory(LeaveRequest::class)->states(['oneDay', 'casual'])->make();

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($normalLeave);

        // then
        $this->assertRegExp('/leave-request\/\d+/', $response->currentUri);
        $this->seeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'reviewing',
            'employee_id' => $employee->id,
            'reason'      => $normalLeave->reason,
            'leave_start' => $normalLeave->leave_start,
            'leave_end'   => $normalLeave->leave_end,
        ])
        ->seeText('Send')
        ->seeText('Cancel Request');

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($casualLeave);


        // then
        $this->assertRegExp('/leave-request\/\d+/', $response->currentUri);
        $this->seeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'accepted',
            'employee_id' => $employee->id,
            'reason'      => $casualLeave->reason,
            'leave_start' => $casualLeave->leave_start,
            'leave_end'   => $casualLeave->leave_end,
        ])
        ->seeText("This Request is Accepted, you can't send any message to the admin regarding this request.");
    }

    /** @test */
    public function leave_request_end_date_may_not_be_before_start_date()
    {
        // given
        $this->withExceptionHandling()->signIn();

        $leave_start = Carbon::now()->next(Carbon::MONDAY);
        $leave_end = (clone $leave_start)->subDays(1);

        $leaveRequest = factory(LeaveRequest::class)->make([ 'leave_start' => $leave_start, 'leave_end' => $leave_end]);

        // when
        $this->post(route('leave-request.store'), $leaveRequest->toArray());
        
        // then
        $this->assertSessionHasErrors(['leave_end']);

        // when
        $this->createLeaveRequest($leaveRequest);

        // then
        $this->seeRouteIs('leave-request.create')
             ->seeText('The leave end must be a date after or equal leave start.');
    }

    /** @test */
    public function leave_requests_made_by_admin_must_be_accepted_by_default()
    {
        // given
        $this->signIn($admin = factory(User::class)->states('admin')->create());

        $leaveRequest = factory(LeaveRequest::class)->make();

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        $leaveRequestId = explode('/', $response->currentUri)[4];

        // then
        $this->seeInDatabase($this->leavesTable, [
            'id'          => $leaveRequestId,
            'employee_id' => $admin->id,
            'status'      => 'accepted',
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ]);

        $admin = $admin->fresh();
        
        $this->assertEquals(7, $admin->casual_leaves);
        $this->assertEquals(14-WeekDaysDuration::get($leaveRequest), $admin->normal_leaves);
    }

    /** @test */
    public function an_employee_may_not_request_more_than_the_allowed_number_of_normal_leaves()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        $leaveRequest = factory(LeaveRequest::class)->make([
            'leave_start' => Carbon::now()->next(Carbon::SUNDAY),
            'leave_end' => Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::SUNDAY)->next(Carbon::SUNDAY)->next(Carbon::SUNDAY),
        ]);

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        // then
        $this->seeRouteIs('leave-request.create');
        $this->notSeeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'reviewing',
            'employee_id' => $employee->id,
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ])
        ->seeText("You can't request 16 days of normal leave.");
    }

    /** @test */
    public function an_employee_may_not_request_more_than_the_allowed_number_of_casual_leaves()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        $leaveRequest = factory(LeaveRequest::class)->states('casual')->make([
            'leave_start' => Carbon::now()->next(Carbon::SUNDAY),
            'leave_end' => Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::SUNDAY)->next(Carbon::SUNDAY),
        ]);

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        // then
        $this->seeRouteIs('leave-request.create');
        $this->notSeeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'accepted',
            'employee_id' => $employee->id,
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ])
        ->seeText("You can't request 11 days of casual leave.");
    }

    /** @test */
    public function an_employee_may_not_request_a_not_defined_leave_request()
    {
        // given
        $this->signIn();
        $mockedRequest = Mockery::mock(Illuminate\Http\Request::class);
        $mockedRequest->shouldReceive('type')->andReturn('FooBar');

        // when
        $result = auth()->user()->can('create', LeaveRequest::class);

        // then
        $this->assertFalse($result);
    }

    /** @test */
    public function an_employee_can_request_unpaid_leave()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        $leaveRequest = factory(LeaveRequest::class)->states('unpaid')->make();

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest($leaveRequest);

        // then
        $this->assertRegExp('/leave-request\/\d+/', $response->currentUri);
        $this->seeInDatabase( $this->leavesTable, [
            'id'          => $leaveRequestId,
            'status'      => 'reviewing',
            'employee_id' => $employee->id,
            'reason'      => $leaveRequest->reason,
            'leave_start' => $leaveRequest->leave_start,
            'leave_end'   => $leaveRequest->leave_end,
        ])
        ->seeText('Send')
        ->seeText('Cancel Request');
    }

    /** @test */
    public function a_notification_must_be_sent_to_admins_after_requesting_leave()
    {
        // given
        Notification::fake();

        $admins = factory(User::class, 3)->states('admin')->create();
        $this->signIn($employee = factory(User::class)->create());

        // when
        list($response, $leaveRequestId) = $this->createLeaveRequest();

        // then
        $leaveRequest = LeaveRequest::find($leaveRequestId);

        Notification::assertSentTo(
            $admins,
            EmployeeRequestedLeave::class,
            function($notification, $channels) use ($leaveRequest) {
                return $notification->leaveRequest->id === $leaveRequest->id;
            }
        );
    }

    /** @test */
    public function it_caches_all_leave_types()
    {
        $leaveTypes = LeaveType::orderBy('id')->get();
        $this->signIn();

        $this->visitRoute('leave-request.create');

        $this->assertEquals($leaveTypes, cache('leaveTypes'));
    }
}
