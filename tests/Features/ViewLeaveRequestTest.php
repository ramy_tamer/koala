<?php

namespace Tests\Features;

use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveRequest;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewLeaveRequestTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function the_admin_sees_the_total_number_of_current_reviewing_requests()
    {
        // given
        $this->signIn($admin = factory(User::class)->states('admin')->create());

        factory(LeaveRequest::class, 3)->create();
        factory(LeaveRequest::class, 4)->states('casual', 'accepted')->create();
        factory(LeaveRequest::class, 5)->states('accepted')->create();

        // when
        $this->visitRoute('home');

        // then
        $this->see('Leave Requests <span class="badge">3</span>');

    }

    /** @test */
    public function only_admin_can_view_leave_requests_waiting_for_review()
    {
        // given
        $this->signIn();

        // when
        $this->visitRoute('leave-request.index');

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't view leave requests.");
    }

    /** @test */
    public function admin_can_view_leave_requests_waiting_for_review()
    {
        // given
        $leaveRequests = factory(LeaveRequest::class, 3)->create();
        $this->signIn($admin = factory(User::class)->states('admin')->create());

        // when
        $this->visitRoute('leave-request.index');

        // then
        $this->seeRouteIs('leave-request.index')
             ->seeTextInOrder(array_map(function($leaveRequest) {
                return str_limit( nl2br(e($leaveRequest['reason'])) , 40);
             }, $leaveRequests->toArray()))
             ->seeText($this->response->getContent(), $leaveRequests->map(function($leaveRequest) {
                return $leaveRequest->reason;
             }));
    }

    /** @test */
    public function an_employee_may_not_view_another_employee_leave_request()
    {
        // given
        $this->signIn($employee = factory(User::class)->create());
        $leaveRequest = factory(LeaveRequest::class)->create();

        // when
        $this->visitRoute('leave-request.show', $leaveRequest->id);

        // then
        $this->seeRouteIs('home');
    }

    /** @test */
    public function an_admin_can_view_all_other_employees_leave_request()
    {
        // given
        $this->signIn($admin = factory(User::class)->states('admin')->create());
        $leaveRequests = factory(LeaveRequest::class, 3)->create();

        // when
        $this->visitRoute('leave-request.show', $leaveRequests[0]->id);
        // then
        $this->seeText($leaveRequests[0]->reason);

        // when
        $this->visitRoute('leave-request.show', $leaveRequests[1]->id);
        // then
        $this->seeText($leaveRequests[1]->reason);

        // when
        $this->visitRoute('leave-request.show', $leaveRequests[2]->id);
        // then
        $this->seeText($leaveRequests[2]->reason);
    }
}
