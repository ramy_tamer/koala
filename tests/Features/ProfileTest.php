<?php

namespace Tests\Features;

use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveRequest;
use Faker\Factory as Faker;
use Koala\WeekDaysDuration;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function an_employee_can_view_his_profile()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());
        
        $nextSunday      = Carbon::now()->next(Carbon::SUNDAY);
        $nextSundayOne   = Carbon::now()->next(Carbon::SUNDAY);
        $nextSundayTwo   = Carbon::now()->next(Carbon::SUNDAY);
        $nextSundayThree = Carbon::now()->next(Carbon::SUNDAY);

        $leaveRequests = [
            factory(LeaveRequest::class)->create(['employee_id' => $employee->id, 'leave_start' => $nextSunday, 'leave_end' => $nextSunday]),
            factory(LeaveRequest::class)->create(['employee_id' => $employee->id, 'leave_start' => $nextSundayOne, 'leave_end' => $nextSundayOne]),
            factory(LeaveRequest::class)->create(['employee_id' => $employee->id, 'leave_start' => $nextSundayTwo, 'leave_end' => $nextSundayTwo]),
            factory(LeaveRequest::class)->create(['employee_id' => $employee->id, 'leave_start' => $nextSundayThree, 'leave_end' => $nextSundayThree]),
        ];

        // when
        $this->visitRoute('profile', $employee->id);

        // then
        $this->seeTextInOrder(
            $employee->fresh()
                     ->leaveRequests()
                     ->latest()
                     ->get()
                     ->map(function($leaveRequest){
                        return str_limit( nl2br(e($leaveRequest->reason)) , 40);
                    })
                    ->toArray()
        );
    }

    /** @test */
    public function an_employee_cannot_view_others_profile()
    {
        // given
        list($firstEmployee, $otherEmployee) = factory(User::class, 2)->states('employee')->create();

        $this->signIn($otherEmployee);

        // when
        $this->visitRoute('profile', $firstEmployee->id);

        // then
        $this->seeRouteIs('home')
             ->seeText("You can't view other employees profiles.");
    }

    /** @test */
    public function an_admin_can_view_any_employee_profile()
    {
        // given
        $employee = factory(User::class)->states('employee')->create();
        $admin = factory(User::class)->states('admin')->create();

        $leaveRequests = factory(LeaveRequest::class, 4)->create(['employee_id' => $employee->id]);

        $this->signIn($admin);

        // when
        $this->visitRoute('profile', $employee->id);

        // then
        $this->seeTextInOrder(
            $employee->fresh()
                     ->leaveRequests()
                     ->latest()
                     ->get()
                     ->map(function($leaveRequest){
                        return str_limit( nl2br(e($leaveRequest->reason)) , 40);
                    })
                    ->toArray()
        );
    }

    /** @test */
    public function an_employee_should_see_a_message_if_he_has_no_logs()
    {
        // given
        $this->signIn($employee = factory(User::class)->states('employee')->create());

        // when
        $this->visitRoute('profile', $employee->id);

        // then
        $this->seeText("You don't have any logs right now.");
    }

    /** @test */
    public function an_admin_should_see_a_message_if_the_employee_has_no_logs()
    {
        // given
        $employee = factory(User::class)->states('employee')->create();
        $admin = factory(User::class)->states('admin')->create();
        $this->signIn($admin);

        // when
        $this->visitRoute('profile', $employee->id);

        // then
        $this->seeText("{$employee->name} doesn't have any logs right now.");
    }

    /** @test */
    public function an_employee_can_see_his_total_number_of_leaves_table()
    {
        $nextSunday = Carbon::now()->next(Carbon::SUNDAY);

        // given
        $this->signIn();

        $normal = factory(LeaveRequest::class)->states('accepted')
            ->create([  'employee_id' => auth()->id(),
                        'leave_start' => (clone $nextSunday),
                        'leave_end' => (clone $nextSunday)->addDays(1)
            ]); // 2 days normal leave
        $casual = factory(LeaveRequest::class)->states('casual', 'accepted')
            ->create([  'employee_id' => auth()->id(),
                        'leave_start' => (clone $nextSunday),
                        'leave_end' => (clone $nextSunday)
                    ]); // 1 day casual leave
        $unpaid = factory(LeaveRequest::class)->states('unpaid', 'accepted')
            ->create([  'employee_id' => auth()->id(),
                        'leave_start' => (clone $nextSunday),
                        'leave_end' => (clone $nextSunday)->next(Carbon::SUNDAY)
                    ]); // 6 days unpaid leave
        $maternity = factory(LeaveRequest::class)->states('maternity', 'accepted')
            ->create([  'employee_id' => auth()->id(),
                        'leave_start' => (clone $nextSunday),
                        'leave_end' => (clone $nextSunday)->next(Carbon::MONDAY)
                    ]); // 7 days maternity leave
        $sick = factory(LeaveRequest::class)->states('sick', 'accepted')
            ->create([  'employee_id' => auth()->id(),
                        'leave_start' => (clone $nextSunday),
                        'leave_end' => (clone $nextSunday)->next(Carbon::TUESDAY)
                    ]); // 7 days maternity leave

        // when
        $this->visitRoute('profile', auth()->id());

        // then
        $this->seeTextInOrder(
            'Normal', WeekDaysDuration::get($normal),
            'Casual', WeekDaysDuration::get($casual),
            'Unpaid', WeekDaysDuration::get($unpaid),
            'Maternity', WeekDaysDuration::get($maternity),
            'Sick', WeekDaysDuration::get($sick)
        );
    }
}
