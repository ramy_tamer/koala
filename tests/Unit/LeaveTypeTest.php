<?php

namespace Tests\Unit;

use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LeaveTypeTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_has_many_leave_requests()
    {
        factory(LeaveRequest::class)->create();

        $this->assertInstanceOf(leaveRequest::class, $this->normalType->leaveRequests[0]);
    }

    /** @test */
    public function it_displays_the_title_of_the_type_when_casted_to_string()
    {
    	$this->assertEquals('Normal Leave [Paid]', $this->normalType);
    	$this->assertEquals('Casual Leave [Paid]', $this->casualType);
    	$this->assertEquals('Unpaid Leave [Unpaid]', $this->unpaidType);
    }

}
