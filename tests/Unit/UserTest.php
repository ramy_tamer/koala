<?php

namespace Tests\Unit;

use Koala\User;
use Carbon\Carbon;
use Koala\Message;
use Tests\TestCase;
use Koala\LeaveRequest;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function it_has_messages()
    {
        $message = factory(Message::class)->create(['sender_id' => $this->user->id]);

        $this->assertInstanceOf(Message::class, $this->user->messages->first());
        $this->assertEquals($message->id, $this->user->messages->first()->id);
    }

    /** @test */
    public function it_has_leaveRequets()
    {
        $leaveRequest = factory(LeaveRequest::class)->create(['employee_id' => $this->user->id]);

        $this->assertInstanceOf(LeaveRequest::class, $this->user->leaveRequests->first());
        $this->assertEquals($leaveRequest->id, $this->user->leaveRequests->first()->id);
    }

    /** @test */
    public function it_can_fetch_unpaid_leave_requests()
    {
        // given
        $unpaidLeaveRequests = factory(LeaveRequest::class, 3)->states('unpaid')->create(['employee_id' => $this->user->id]);
        $normalLeaveRequests = factory(LeaveRequest::class, 4)->create(['employee_id' => $this->user->id]);
        $casualLeaveRequests = factory(LeaveRequest::class, 5)->states('casual')->create(['employee_id' => $this->user->id]);

        $userUnpaidLeaveRequests = $this->user->fresh()->leaveRequests()->is($this->unpaidType)->pluck('id');

        // when
        $this->assertEquals(3, $unpaidLeaveRequests->pluck('id')->intersect($userUnpaidLeaveRequests)->count());
        $this->assertEquals(0, $normalLeaveRequests->pluck('id')->intersect($userUnpaidLeaveRequests)->count());
        $this->assertEquals(0, $casualLeaveRequests->pluck('id')->intersect($userUnpaidLeaveRequests)->count());

        $this->assertEquals($userUnpaidLeaveRequests, $unpaidLeaveRequests->pluck('id'));
    }

    /** @test */
    public function it_should_calcualte_the_number_of_days_normal_leave_requests()
    {
        // given
        factory(LeaveRequest::class)->states('accepted')->create(['employee_id' => $this->user->id]);
        factory(LeaveRequest::class)->states('accepted')->create([
            'employee_id' => $this->user->id,
            'leave_start' => Carbon::now(),
            'leave_end' => Carbon::now(),
        ]);

        // when
        $normalLeaveRequests = $this->user->calculatedNormalLeaves;

        // then
        $this->assertEquals(4, 14 - $normalLeaveRequests);
    }

    /** @test */
    public function it_should_calcualte_the_number_of_days_casual_leave_requests()
    {
        // given
        factory(LeaveRequest::class)->states('casual', 'accepted')->create(['employee_id' => $this->user->id]);
        factory(LeaveRequest::class)->states('casual', 'accepted')->create([
            'employee_id' => $this->user->id,
            'leave_start' => Carbon::now(),
            'leave_end' => Carbon::now(),
        ]);

        // when
        $casualLeaveRequests = $this->user->calculatedCasualLeaves;

        // then
        $this->assertEquals(4, 7 - $casualLeaveRequests);
    }

    /** @test */
    public function it_should_calcualte_the_number_of_days_unpaid_leave_requests()
    {
        // given
        factory(LeaveRequest::class)->states('accepted', 'unpaid')->create(['employee_id' => $this->user->id]);
        factory(LeaveRequest::class)->states('accepted', 'unpaid')->create([
            'employee_id' => $this->user->id,
            'leave_start' => Carbon::now(),
            'leave_end' => Carbon::now(),
        ]);

        // when
        $unpaidLeaveRequests = $this->user->calculatedUnpaidLeaves;

        // then
        $this->assertEquals(4, $unpaidLeaveRequests);
    }

    /** @test */
    public function it_should_calcualte_the_number_of_days_sick_leave_requests()
    {
        // given
        factory(LeaveRequest::class)->states('accepted')->create(['employee_id' => $this->user->id, 'leave_type_id' => $this->sickType->id]);
        factory(LeaveRequest::class)->states('accepted')->create([
            'employee_id'   => $this->user->id,
            'leave_start'   => Carbon::now(),
            'leave_end'     => Carbon::now(),
            'leave_type_id' => $this->sickType->id
        ]);

        // when
        $sickLeaveRequests = $this->user->leavesCount($this->sickType);

        // then
        $this->assertEquals(4, $sickLeaveRequests);
    }

}
