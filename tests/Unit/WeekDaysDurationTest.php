<?php

namespace Tests\Unit;

use Koala\User;
use Carbon\Carbon;
use Koala\Message;
use Tests\TestCase;
use Koala\LeaveRequest;
use Koala\WeekDaysDuration;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WeekDaysDurationTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->requestMock = new \stdClass;
    }

    /** @test */
    public function it_should_calculate_same_day_as_one_day()
    {
        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY);

        $this->assertEquals(1, WeekDaysDuration::get($this->requestMock));
    }

    /** @test */
    public function it_calculates_the_number_of_working_days_between_two_dates()
    {
        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::MONDAY);

        $this->assertEquals(2, WeekDaysDuration::get($this->requestMock));

        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::TUESDAY);

        $this->assertEquals(3, WeekDaysDuration::get($this->requestMock));

        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::THURSDAY);

        $this->assertEquals(5, WeekDaysDuration::get($this->requestMock));

    }

    /** @test */
    public function it_shouldnot_calculate_weekends()
    {
        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::FRIDAY);

        $this->assertEquals(5, WeekDaysDuration::get($this->requestMock));

        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::SATURDAY);

        $this->assertEquals(5, WeekDaysDuration::get($this->requestMock));

        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->next(Carbon::SUNDAY);

        $this->assertEquals(6, WeekDaysDuration::get($this->requestMock));

        $this->requestMock->leave_start = Carbon::now()->next(Carbon::SUNDAY);
        $this->requestMock->leave_end   = Carbon::now()->next(Carbon::SUNDAY)->addDays(8);

        $this->assertEquals(7, WeekDaysDuration::get($this->requestMock));
    }
}
