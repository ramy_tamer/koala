<?php

namespace Tests\Unit;

use Koala\User;
use Koala\Message;
use Tests\TestCase;
use Koala\LeaveRequest;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessageTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->message = factory(Message::class)->create();        
    }

    /** @test */
    public function it_belongs_to_a_leave_request()
    {
        $this->assertInstanceOf(LeaveRequest::class, $this->message->leaveRequest);
    }

    /** @test */
    public function it_belongs_to_a_sender()
    {
        $this->assertInstanceOf(User::class, $this->message->sender);
    }

}
