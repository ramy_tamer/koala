<?php

namespace Tests\Unit;

use Koala\User;
use Carbon\Carbon;
use Tests\TestCase;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LeaveRequestTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->leaveRequest = factory(LeaveRequest::class)->create();        
    }

    /** @test */
    public function it_belongs_to_a_leave_type()
    {
        $this->assertInstanceOf(LeaveType::class, $this->leaveRequest->type);
    }

    /** @test */
    public function it_belongs_to_an_employee()
    {
        $this->assertInstanceOf(User::class, $this->leaveRequest->employee);
    }

    /** @test */
    public function it_has_an_admin_with_type_user()
    {
        $leaveRequest = factory(LeaveRequest::class)->states('admin')->create();
        $this->assertInstanceOf(User::class, $leaveRequest->admin);
    }

    /** @test */
    public function it_calculates_the_duration_of_the_leave_request()
    {
        $leave_start = Carbon::parse('2017-04-02'); // sunday

        $oneDayLeave = factory(LeaveRequest::class)->create([
            'leave_start' => $leave_start,
            'leave_end' => (clone $leave_start)
        ]);
        $twoDaysLeave = factory(LeaveRequest::class)->create([
            'leave_start' => $leave_start,
            'leave_end' => (clone $leave_start)->addDays(1)
        ]);
        $threeDaysLeave = factory(LeaveRequest::class)->create([
            'leave_start' => $leave_start,
            'leave_end' => (clone $leave_start)->addDays(2)
        ]);

        $this->assertEquals(1, $oneDayLeave->duration, "{$threeDaysLeave->leave_start} - {$threeDaysLeave->leave_end}");
        $this->assertEquals(2, $twoDaysLeave->duration, "{$threeDaysLeave->leave_start} - {$threeDaysLeave->leave_end}");
        $this->assertEquals(3, $threeDaysLeave->duration, "{$threeDaysLeave->leave_start} - {$threeDaysLeave->leave_end}");
    }

    /** @test */
    public function it_calculates_the_duration_of_the_leave_request_correctly_without_holidays()
    {   
        // duration should be 3 days (3 working days)
        $thursday = Carbon::parse('2017-04-06');
        $monday   = Carbon::parse('2017-04-10');

        $leaveRequest = factory(LeaveRequest::class)->create(['leave_start' => $thursday, 'leave_end' => $monday]);

        $this->assertEquals(3, $leaveRequest->duration);
    }

    /** @test */
    public function it_fetches_only_reviewing_requests()
    {
        LeaveRequest::getQuery()->delete();
        $reviwingLeaveRequests = factory(LeaveRequest::class, 2)->create();
        $rejectedLeaveRequests = factory(LeaveRequest::class, 3)->states('rejected')->create();
        $acceptedLeaveRequests = factory(LeaveRequest::class, 3)->states('accepted')->create();

        $this->assertEquals(2, LeaveRequest::requests()->count());
        $this->assertEquals(8, LeaveRequest::all()->count());
    }

    /** @tests */
    public function it_fetches_only_accepted_requests()
    {
        LeaveRequest::getQuery()->delete();
        $reviwingLeaveRequests = factory(LeaveRequest::class, 2)->create();
        $rejectedLeaveRequests = factory(LeaveRequest::class, 3)->states('rejected')->create();
        $acceptedLeaveRequests = factory(LeaveRequest::class, 3)->states('accepted')->create();

        $this->assertEquals(3, LeaveRequest::accepted()->count());
        $this->assertEquals(8, LeaveRequest::all()->count());
    }

}
