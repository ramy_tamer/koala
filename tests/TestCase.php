<?php

namespace Tests;

use Koala\User;
use Koala\LeaveType;
use Koala\LeaveRequest;
use Koala\Exceptions\Handler;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\Constraints\PageConstraint;
use Illuminate\Foundation\Testing\TestCase as LaravelTestCase;

abstract class TestCase extends LaravelTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost:8000';

    protected $loggedInUsers = [];

    protected function setUp()
    {
        parent::setUp();
        $this->disableExceptionHandling();

        $this->normalType    = LeaveType::whereTitle('normal')->first();
        $this->casualType    = LeaveType::whereTitle('casual')->first();
        $this->unpaidType    = LeaveType::whereTitle('unpaid')->first();
        $this->maternityType = LeaveType::whereTitle('maternity')->first();
        $this->sickType      = LeaveType::whereTitle('sick')->first();

        // dd($this->app->environment(), $this->app->environmentFilePath());
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        // var_dump($app->environment(), $app->environmentFilePath(), env('MAIL_DRIVER'));

        return $app;
    }

    public function signIn(User $user = null)
    {   
        $this->loggedInUsers[] = $user ?? factory(User::class)->create();

        $this->actingAs(end($this->loggedInUsers));

        return $this;
    }

    protected function createLeaveRequest($leaveRequest = null)
    {
        $leaveRequest = $leaveRequest ?? factory(LeaveRequest::class)->make();
        
        $response =  $this->visitRoute('home')
                           ->click('request-leave')
                           ->seeRouteIs('leave-request.create')
                           ->select($leaveRequest->type->id, 'leave_type_id')
                           ->storeInput('reason', $leaveRequest->reason)
                           ->storeInput('leave_start', $leaveRequest->leave_start)
                           ->storeInput('leave_end', $leaveRequest->leave_end)
                           ->press('Submit Request');

        $explodedUrl = explode('/', $response->currentUri);

        return [$response, end($explodedUrl)];
    }

    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}
            public function report(\Exception $e) {}
            public function render($request, \Exception $e) {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);
        return $this;
    }

    public function seeTextInOrder($expected)
    {
        if(! is_array($expected)) {
            $expected = func_get_args();
        }
        
        return $this->assertInPage(new HasTextInOrder($expected));
    }

    public function dontSeeTextInOrder($expected)
    {
        if(! is_array($expected)) {
            $expected = func_get_args();
        }
        
        return $this->assertInPage(new HasTextInOrder($expected), $negate = true);
    }
}


class HasTextInOrder extends PageConstraint {
    /**
     * The expected sequance.
     *
     * @var array
     */
    protected $expected;

    /**
     * Create a new constraint instance.
     *
     * @param  array  $text
     * @return void
     */
    public function __construct($expected)
    {
        $this->expected = $expected;
    }

    /**
     * Check if the plain text sequance is found in the given crawler.
     *
     * @param  \Symfony\Component\DomCrawler\Crawler|string  $crawler
     * @return bool
     */
    protected function matches($crawler)
    {
        $escapedSequance = array_map(function($text) {
            return $this->getEscapedPattern($text);
        }, $this->expected);

        $pattern = join('[\s\S]*', $escapedSequance);

        return preg_match("/{$pattern}/i", $this->text($crawler));
    }

    /**
     * Returns a string representation of the object.
     *
     * @return string
     */
    public function toString()
    {   
        $text = join(', ', $this->expected);

        return "the sequance [{$text}]";
    }
}